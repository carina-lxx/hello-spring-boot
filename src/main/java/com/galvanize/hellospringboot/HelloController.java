package com.galvanize.hellospringboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(@RequestParam(defaultValue = "World", required = false) String name) {
        return String.format("Hello %s", name);
    }

    @GetMapping("/operator")
    public String operate(@RequestParam List<String> input) {
        int res = 0;
        String operator = input.get(input.size() - 1);
        input.remove(input.size() - 1);

        switch(operator) {
            case "+":
                for(int i = 0; i < input.size(); i++) {
                    res += Integer.parseInt(input.get(i));
                }
                break;

            case"-":
                for(int i = 0; i < input.size(); i++) {
                    res -= Integer.parseInt(input.get(i));
                }
                break;

            case"*":
                for(int i = 0; i < input.size(); i++) {
                    res *= Integer.parseInt(input.get(i));
                }
                break;

            case"/":
                for(int i = 0; i < input.size(); i++) {
                    res /= Integer.parseInt(input.get(i));
                }
                break;

            default:
                return "input is not valid";
        }
        return "returned value is " + res;
    }

    @GetMapping("/count")
    public String count(@RequestParam String input) {
        Set<Character> dic = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u'));

        int res = 0;
        for(int i = 0; i < input.length(); i++) {
            if (dic.contains(input.charAt(i))) {
                res++;
            }
        }
        return "returned number of vowels is " + res;
    }

    @GetMapping("/replace")
    public String replace(@RequestParam Map<String, String> params) {
        String body = params.get("body");
        String word1 = params.get("word1");
        String word2 = params.get("word2");

        return body.replaceAll(word1, word2);
    }
}
