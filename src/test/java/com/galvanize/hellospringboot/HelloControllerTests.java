package com.galvanize.hellospringboot;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@WebMvcTest(HelloController.class) // add controller name
public class HelloControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    @DisplayName("When localhost:8080/hello should return Hello World")
    void sayHello_noParam_rtnHelloWorld() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/hello"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello World"));
    }

    @Test
    @DisplayName("When localhost:8080/hello?name = someone should return Hello someone")
    void sayHello_myName_rtnHelloName() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/hello?name=Carina"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello Carina"));
    }

    @Test
    @DisplayName("When localhost:8080/hello/operator?input=1,2,3,+ should return 6")
    void operator_returnValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/operator?input=1,2,3,+"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("returned value is 6"));
    }


    @Test
    @DisplayName("When localhost:8080/count/vowels?input=apple should return 2")
    void count_returnValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/count?input=apple"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("returned number of vowels is 2"));
    }

    @Test
    @DisplayName("When localhost:8080/replace?body=welcome join&word1=join&word2=home should return welcome home")
    void replace_returnValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/replace?body=welcome join&word1=join&word2=home"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("welcome home"));
    }
}
